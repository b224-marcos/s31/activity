
let http = require("http");

const port = 3000;

http.createServer(function(request, response){
	if(request.url == "/login"){
		response.writeHead(200, {"Content-Type": "text/plain"});
		response.end("You are in the login page.")
	}
	else{
		response.writeHead(404, {"Content-Type": "text/plain"});
		response.end("Page not Available")
	}
}).listen(3000);


console.log("Server is running at localhost:3000");
